# staticwiki

This is a very small program that converts a [minwiki](https://bitbucket.org/bachmeil/minwiki)
directory into a static website.

# Requirements

You need a D compiler and Pandoc installed on your machine.

# Installation

There's not much to do for "installation". Compile the single source file:

```
dmd staticwiki.d -J.
```

The `-J.` facilitates reading in the Pandoc template stored in template.html.
To change the template, edit template.html however you want. If 
template.html does not already exist in your project directory, it will 
be created. The idea is to create a single, self-contained binary 
without having to think about the template file. That way there is no 
cognitive overhead associated with the creation of your 
wiki and converting it to a static site.

For convenience, you probably want to symlink the binary to a directory 
in your PATH, for instance

```
ln -s $(pwd)/staticwiki $HOME/bin
```

# Usage

Run staticwiki inside a directory holding markdown files produced by
minwiki:

```
staticwiki
```

It will create a new directory (static/ by default, but that can be changed
by setting `staticdir` inside staticwiki.d prior to compilation) holding
html files.

# What the program does

Aside from creating html files (a trivial task using Pandoc that takes
only a few lines), the main thing staticwiki does is convert internal
minwiki links of the form `[#foo | My foo story]` and convert them into
regular markdown links with an html extension, like `[My foo story](foo.html)`.
The main complication is the need to avoid changing links inside code
blocks.
