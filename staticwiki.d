import std.exception, std.file, std.path, std.process, std.stdio, std.string;

enum staticdir = "static";
enum pandocTemplate = import("template.html");

void main() {
	if (!exists(staticdir)) {
		mkdir(staticdir);
	}
	if (!exists("template.html")) {
		std.file.write("template.html", pandocTemplate);
	}
	foreach(f; std.file.dirEntries(".", SpanMode.shallow)) {
		if (extension(f) == ".md") {
			writeln(f);
			string content = readText(f);
			std.file.write(staticdir ~ "/" ~ baseName(f), textChunks(content));
			string cmd = "pandoc -s " ~ staticdir ~ "/" ~ baseName(f) ~ " -o "
				~ staticdir ~ "/" ~ setExtension(baseName(f), "html") ~ 
				" --template=template.html";
			writeln(cmd);
			executeShell(cmd);
		}
	}
}

string replaceLinks(string s, string result="") {
	long ind = s.indexOf("[#");
	if (ind == -1) {
		return result ~ s;
	}
	
	long ind2 = s.indexOf("]", ind+2);
	enforce(ind2 >= 0, "Unclosed internal link");
	string[] linkInfo = s[ind+2..ind2].split("|");
	string newlink;
	if (linkInfo.length == 1) {
		newlink = "[" ~ linkInfo[0].strip ~ "](" ~ linkInfo[0].strip ~ ".html)";
	} else {
		newlink = "[" ~ linkInfo[1].strip ~ "](" ~ linkInfo[0].strip ~ ".html)";
	}
	return replaceLinks(s[ind2+1..$], result ~ s[0..ind] ~ newlink);
}

string textChunks(string s, string result="") {
	if (s.length == 0) {
		return result;
	}

	if (s.startsWith("```\n")) {
		long ind = s.indexOf("\n```\n", 4);
		if (ind == -1) {
			ind = s.indexOf("\n```", 4);
		}
		enforce(ind >= 0, "Unclosed code block");
		return textChunks(s[ind+4..$], result ~ "\n" ~ s[0..ind+4]);
	} 

	if (s.startsWith("\n```\n")) {
		long ind = s.indexOf("\n```\n", 5);
		if (ind == -1) {
			ind = s.indexOf("\n```", 5);
		}
		enforce(ind >= 0, "Unclosed code block");
		return textChunks(s[ind+5..$], result ~ "\n" ~ s[0..ind+5]);
	} 
	
	long ind = s.indexOf("\n```\n");
	if (ind == -1) {
		return textChunks("", result ~ s.replaceLinks());
	} else {
		return textChunks(s[ind..$], result ~ s[0..ind].replaceLinks());
	}
}
